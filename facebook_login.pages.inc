<?php

/**
 * Facebook login setting form.
 */
function facebook_login_setting_form($form, &$form_state) {
  $fb_app_url = 'http://www.facebook.com/developers/apps.php';
  $form['facebook_login_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook App ID'),
    '#default_value' => variable_get('facebook_login_app_id', ''),
    '#description' => t('Set up your app in <a href="' . $fb_app_url . '">my apps</a> on Facebook. Enter your App ID here.'),
    '#required' => TRUE,
  );
  $form['facebook_login_app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook App Secret'),
    '#default_value' => variable_get('facebook_login_app_secret', ''),
    '#description' => t('Set up your app in <a href="' . $fb_app_url . '">my apps</a> on Facebook. Enter your App Secret here.'),
    '#required' => TRUE,
  );
  $form['facebook_login_app_version'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook App Secret'),
    '#default_value' => variable_get('facebook_login_app_version', 'v2.3'),
    '#description' => t('Set up your app in <a href="' . $fb_app_url . '">my apps</a> on Facebook. Enter API Version here.'),
  );
  $form['advance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced permission settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advance']['facebook_login_permissions'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Request additional permission to the following'),
    '#options' => facebook_user_permissions(),
    '#default_value' => variable_get('facebook_login_permissions', facebook_permissions_default()),
  );  
  
  return system_settings_form($form);
}

/**
 * Facebook connect page.
 */
function facebook_login_connect() {
  $destination = drupal_get_destination();
  $redirect_uri = url('facebook/connect/redirect', array(
    'absolute' => TRUE,
    'query' => $destination,
  ));

  $fb = facebook_login_get_api();
  $fbu = facebook_login_fb_facebook_user($fb);
  if (!$fbu) {
    $login_url = facebook_login_get_login_url($redirect_uri);
    header('Location: ' . $login_url, TRUE, 302);
    drupal_exit($login_url);
  }
  else {
    header('Location: ' . $redirect_uri, TRUE, 302);
    drupal_exit($redirect_uri);
  }
}

/**
 * Facebook connect redirect page.
 */
function facebook_login_connect_redirect() {
  global $base_url;
  $fb = facebook_login_get_api();
  $fbu = facebook_login_fb_facebook_user($fb);
  if ($fbu) {
    module_invoke_all('facebook_user_connect', $fbu);
  }
  drupal_goto($base_url);
}

/**
 * Facebook destroy session.
 */
function facebook_login_session() {
  global $base_url;
  $fb = facebook_login_get_api();
  $fb->destroySession();
  drupal_goto($base_url);
}

/**
 * Facebook login page.
 */
function facebook_login_page() {
  global $base_url;
  try {
    $fb = facebook_login_get_api();
    $fbu_id = facebook_login_fb_facebook_user($fb);
    
    if ($fbu_id) {
      facebook_login_user_check_login($fbu_id);
    }
  }
  catch(Exception $e) {
    watchdog('facebook login', $e->__toString(), array(), WATCHDOG_WARNING);
  }
  drupal_goto($base_url);
}
