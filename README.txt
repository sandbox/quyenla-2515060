Drupal for Facebook
-------------------

To install:

- Make sure you have a PHP client from facebook (version >= 3.1.1).
  The 2.x.y versions are not supported by this version of Drupal for Facebook.
  Download from https://github.com/facebook/facebook-php-sdk.
  Extract the files, and place them in sites/all/libraries/facebook-php-sdk.

  If you have the Libraries API module installed, you may place the files in
  another recognised location (such as sites/all/libraries), providing that the
  directory is named 'facebook-php-sdk'.

  Or, to manually set the location of the php-sdk in any other
  directory, edit your settings.php to include a line similar to the
  one below. Add to the section where the $conf variable is defined,
  or the very end of settings.php. And customize the path as needed.

  $conf['fb_api_file'] = 'sites/all/libraries/facebook-php-sdk/src/facebook.php';

  See also http://drupal.org/node/923804

- Your theme needs the following attribute at the end of the <html> tag:

  xmlns:fb="http://www.facebook.com/2008/fbml"


- Your theme needs the following attribute at the end of the <html> tag:

  xmlns:fb="http://www.facebook.com/2008/fbml"

  Drupal 7 should include this by default.  Use your browser's view source feature to confirm.
  If not, you may need to edit your theme's html.tpl.php file.  See
  http://www.drupalforfacebook.org/node/1106.  Note this applies to
  themes used for Facebook Connect, iframe Canvas Pages, and Social
  Plugins (i.e. like buttons).  Without this attribute, IE will fail.

  Note that some documention on facebook.com suggests
  xmlns:fb="http://ogp.me/ns/fb#" instead of the URL above.  Try that
  if the above is not working for you.


- To support canvas pages and/or page tabs, url rewriting and other
  settings must be initialized before modules are loaded, so you must
  add this code to your settings.php.  This is done by adding these
  two lines to the end of sites/default/settings.php (or
  sites/YOUR_DOMAIN/settings.php).
