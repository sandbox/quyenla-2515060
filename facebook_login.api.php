<?php
/**
 * @file
 * This file contains API documentation for the Facebook Login module. Note that
 * all of this code is merely for example purposes, it is never executed when
 * using the Facebook OAuth module.
 */

/**
 * Implements hook_facebook_name_alter().
 */ 
function hook_facebook_name_alter(&$username) {
  $username = facebook_string_unicode_to_ascii($username);
}

/**
 * Implements hook_facebook_login().
 */ 
function hook_facebook_login($account, $facebook_user, $new_account) {
  
}
