(function ($){

  Drupal.behaviors.bayoFacebookLogin = {
    attach: function (context, settings) {
      var scope = settings.facebookScope;
      $('.facebook-login', context).once('facebook-login', function () {
        $(this).click(function(){
          var reload_url = $(this).data('reload-url');
          var appId = settings.facebookAppId;
          var appVesion = settings.facebookAppVesion
          FB.init({
            appId      : appId,
            cookie     : true,
            xfbml      : true,
            version    : appVesion
          });
          FB.login(function(response) {
            console.log(response);
            var auth = response.authResponse;
            var status = response.status;
            if (status === 'connected' && auth !== null) {
              if (typeof(reload_url) == 'undefined' || reload_url.length == 0) {
                if (settings.facebookReloadUri.length) {
                  FB_JS.reload(settings.facebookReloadUri);
                }
                else {
                  FB_JS.reload();
                }
              }
              else {
                FB_JS.reload(reload_url);
              }
            };
          }, {
            scope: scope, 
            return_scopes: true
          });
          return false;
        });
      });
    }
  };

  Drupal.behaviors.bayoFacebookInvite = {
    attach: function(context, settings) {
      $('.facebook-invite', context).once('facebook-invite', function() {
        $(this).click(function(){
          var fb_id = $(this).data('fb-id');
          var message = $(this).data('invite-msg');
          var options = {
            method: 'apprequests',
            message: message
          };
          if (typeof fb_id != 'undefined') {
            options.to = fb_id;
          }
          FB.ui(options, function(response){
            console.log(response);
          });
        });
      });
    }
  };

  Drupal.behaviors.bayoFacebookSend = {
    attach: function(context, settings) {
      $('.facebook-send', context).once('facebook-send', function() {
        $(this).click(function(){
          var fb_id = $(this).data('fb-id');
          var link = $(this).data('link');
          FB.ui({
            method: 'send',
            to: fb_id,
            link: link
          });
        });
      });
    }
  };

  Drupal.behaviors.bayoFacebookShare = {
    attach: function(context, settings) {
      $('.facebook-share', context).once('facebook-share', function() {
        $(this).click(function(){
          var href = $(this).data('href');
          var callback = $(this).data('callback');
          FB.ui({
            method: 'share',
            href: href
          }, function(response){
            console.log(response);
          });
        });
      });

      $('.facebook-popup-share', context).once('facebook-share', function() {
        $(this).click(function(){
          var href = $(this).data('href');
          window.open('https://www.facebook.com/sharer/sharer.php?u=' + href, 'sharer', 'width=500,height=400,top=200,left=400');
        });
      });
    }
  };

})(jQuery);
